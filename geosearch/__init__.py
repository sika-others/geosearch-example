from django.contrib.gis.geos import GEOSGeometry

from models import GSPoint
from helpers import get_grid

def add_point(key, point, tag=None):
    point = GSPoint(key=key, point="POINT(%s %s)"%point, tag=tag)
    point.save()
    return point

def remove_point(key):
    GSPoint.objects.get(key=key).delete()

def get_points_from_rectangle(a, b):
    polygon = "POLYGON((%s %s, %s %s, %s %s, %s %s, %s %s))" % (
        a[0], a[1], b[0], a[1], b[0], b[1], a[0], b[1], a[0], a[1], )
    return GSPoint.objects.filter(point__contained=GEOSGeometry(polygon))

def get_grouped_from_rectangle(rectangele, v=20, h=5,
                               get_points_from_rectangle=get_points_from_rectangle):
    for r, vi, hi in get_grid(rectangele, v, h):
        qs = get_points_from_rectangle(r[0], r[1])
        if qs.count() == 1:
            yield ("o", qs[0], vi, hi)
        else:
            yield ("c", qs.count(), vi, hi)
            