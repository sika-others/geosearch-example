def get_grid(rectangle, h, v):
    a, b = rectangle
    sv = ((b[1]-a[1])/v)
    sh = ((b[0]-a[0])/h)
    for iv in xrange(v):
        y = (a[1]+iv*sv, a[1]+(iv+1)*sv)
        for ih in xrange(h):
            x = (a[0]+ih*sh, a[0]+(ih+1)*sh)
            na = (x[0], y[0])
            nb = (x[1], y[1])
            yield ((na, nb), ih, iv)