from django.contrib.gis.db import models


class GSPoint(models.Model):
    key = models.CharField(max_length=255, unique=True)
    tag = models.CharField(max_length=255, blank=True, null=True)
    point = models.PointField()

    objects = models.GeoManager()

    def __unicode__(self):
        lon, lat = self.point.coords
        return u"<GSPoint: %s (%s %s)>" % (self.key, round(lon, 3), round(lat, 3))