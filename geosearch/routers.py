POSTGIS_APPS = ("geosearch", )

class GeosearchRouter(object):
    def db_for_read(self, model, **hints):
        if model._meta.app_label in POSTGIS_APPS:
            return "postgis"

    def db_for_write(self, model, **hints):
        if model._meta.app_label in POSTGIS_APPS:
            return "postgis"

    def allow_relation(self, obj1, obj2, **hints):
        return True

    def allow_migrate(self, db, model):
        if db == "default":
            if model._meta.app_label in POSTGIS_APPS:
                return False
            return True
        if db == "postgis":
            if model._meta.app_label in POSTGIS_APPS:
                return True
            return False